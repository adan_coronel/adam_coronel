#include <misc.h>
#include <params.h>

subroutine initcom

    use shr_kind_mod, only: r8 => shr_kind_r8
    use pmgrid
    use dynpkg
    use constituents, only: ppcnst, qmin, qmincg
    use time_manager, only: get_step_size
    use stdatm
    use comhd
    use dycore, only: is_even_area
    use vertical_coordinate
    use advection_scheme

    implicit none

    integer m
    real(r8) dtime

    call setmsa0(TBB, HBB, ghs, P00, T00, PSB, TSB)

    ! TODO: Eliminate "hPa" units!
    p00 = p00*100.0d0 ! convert units from hPa to Pa
    call hycoef()

    dtime = get_step_size()

    call advection_scheme_init()

    call initialize_comhd()

    dthdfs = dtime 

    call stdfsc(dfs0, dthdfs, sinu, sinv, wtgu(beglatexdyn), &
                wtgv(beglatexdyn), dx, dy, &
                frdt, frds, frdu, frdv, frdp, dxvpn, dxvps)

    qmin(1) = 1.0d-12          ! Minimum mixing ratio for moisture
    do m = 2, ppcnst
        qmin(m) = 0.0d0
    end do
!
! Set the minimum mixing ratio for the counter-gradient term.  
! Normally this should be the same as qmin, but in order to 
! match control case 414 use zero for water vapor.
!
    qmincg(1) = 0.
    do m = 2, ppcnst
        qmincg(m) = qmin(m)
    end do
!
! Set flag indicating dynamics grid is now defined.
! NOTE: this ASSUMES initcom is called after spmdinit.  The setting of nlon done here completes
! the definition of the dynamics grid.
!
    dyngrid_set = .true.

end subroutine initcom
